# THREE VR

Template for VR-Enabled ThreeJS projects.  

Includes https://www.npmjs.com/package/broadcast-desktop for viewing development machines's desktop in realtime from your headset. Please visit https://<dev-machine-ip>:8000 in your oculus broswer first and click "proceed". After that go back to https://<dev-machine-ip>:8080 to view your app.   

If hand tracking doesn't work, open the oculus browser to:   
`chrome://flags/#webxr-hands`  
and set to *enabled*  
