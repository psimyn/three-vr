import { MeshNormalMaterial, WebGLRenderer, Scene, PerspectiveCamera, CubeTextureLoader,
  sRGBEncoding, SphereGeometry, BoxGeometry, MeshBasicMaterial, BackSide,
  LinearFilter,
  MeshStandardMaterial, TextureLoader, RepeatWrapping,
  PlaneBufferGeometry, Mesh, Color, Object3D, Fog } from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import { VRButton } from 'three/examples/jsm/webxr/VRButton';
import autoBind from 'auto-bind';
import Lights from './modules/lights';
import Desk from './modules/desk';
import Keyboard from './modules/keyboard';
import Monitor from './modules/monitor';
import Toy from './modules/toy';
import VRConsole from './modules/vr-console';
import Hands from './modules/hands';
import BroadcastDesktop from 'broadcast-desktop/client';
import { XRHandModelFactory } from 'three/examples/jsm/webxr/XRHandModelFactory';
import { FBXLoader } from 'three/examples/jsm/loaders/FBXLoader';

import './index.scss';

// TODO: find a better way to implement and share this
if (window.location.origin.indexOf('localhost') === -1) window.desktop = new BroadcastDesktop();

class App {
  constructor() {
    autoBind(this);

    this.el = document.createElement('div');
    document.body.appendChild(this.el);

    this.scene = new Scene();
    this.scene.name = 'scene';
    this.scene.background = new Color(0x878787);
    this.scene.fog = new Fog(0x878787, 10, 50);
    this.sceneObjects = new Object3D();
    this.sceneObjects.name = 'objects';
    this.scene.add(this.sceneObjects);

    this.camera = new PerspectiveCamera(50, window.innerWidth / window.innerHeight, 0.1, 100);
    this.camera.position.set(0, 1.6, 3);

    this.controls = new OrbitControls(this.camera, document.body);
    this.controls.target.set(0, 1.6, 0);
    this.controls.update();

    const floorGeometry = new PlaneBufferGeometry(100, 100);
    const floorMaterial = new MeshStandardMaterial({ color: 0xeeeeee, roughness: 1.0, metalness: 0.0, map: new TextureLoader().load('assets/images/grid1.png') });
    floorMaterial.map.wrapS = RepeatWrapping;
    floorMaterial.map.wrapT = RepeatWrapping;
    floorMaterial.map.repeat.set(200, 200);

    const floor = new Mesh(floorGeometry, floorMaterial);
    floor.rotation.x = -Math.PI / 2;
    floor.receiveShadow = true;
    floor.name = 'floor';
    this.sceneObjects.add(floor);

    this.renderer = new WebGLRenderer({ antialias: true });
    this.renderer.setPixelRatio(window.devicePixelRatio);
    this.renderer.setSize(window.innerWidth, window.innerHeight);
    this.renderer.shadowMap.enabled = true;
    this.renderer.xr.enabled = true;
    this.renderer.domElement.classList.add('scene');
    this.el.appendChild(this.renderer.domElement);


    // this.headset = new Mesh(new BoxGeometry(1, 0.5, 0.25, 16, 16, 16), new MeshNormalMaterial());

    this.headset = new Object3D();
    this.scene.add(this.headset);
    const headsetLoader = new FBXLoader().load('assets/models/headset1.fbx', (res) => {
      const headset = res.children[0];
      headset.scale.set(0.01, 0.01, 0.01);
      this.headset.add(headset);
    });
    const handModelFactory = new XRHandModelFactory().setPath('assets/models/');

    this.hand1 = this.renderer.xr.getHand(0);
    this.hand2 = this.renderer.xr.getHand(1);

    this.hand1.add(handModelFactory.createHandModel(this.hand1, 'oculus'));
    this.hand2.add(handModelFactory.createHandModel(this.hand2, 'oculus'));

    this.handsArray = [this.hand1, this.hand2];

    this.handsObject = new Object3D();
    this.handsObject.add(this.hand1);
    this.handsObject.add(this.hand2);
    this.scene.add(this.handsObject);


    document.body.appendChild(VRButton.createButton(this.renderer));

    this.sceneModules = [
      { name: 'lights', class: Lights, path: './modules/lights' },
      { name: 'desk', class: Desk, path: './modules/desk' },
      { name: 'keyboard', class: Keyboard, path: './modules/keyboard' },
      { name: 'monitor', class: Monitor, path: './modules/monitor' },
      { name: 'toy', class: Toy, path: './modules/toy' },
      { name: 'hands', class: Hands, path: './modules/hands' },
      { name: 'vrconsole', class: VRConsole, path: './modules/vr-console' },
    ];

    this.sceneObjects.renderer = this.renderer;
    this.sceneObjects.scene = this.scene;

    for (let i = 0; i < this.sceneModules.length; i += 1) {
      const obj = this.sceneModules[i];
      this[obj.name] = new obj.class();
      this[obj.name].object.name = this[obj.name];
      this[obj.name === 'hands' ? 'scene' : 'sceneObjects'].add(this[obj.name].object);
    }

    this.getHandMeshes().then(() => {
      this.hands.setHands(this.handsArray);
    });


    if (module.hot) {
      // why this doesn't work and calling them individually does I can't figure out
      // for (let i = 0; i < this.sceneObjects.length; i += 1) {
      //   const obj = this.sceneObjects[i];
      //   module.hot.accept(obj.path, () => {
      //     this.scene.remove(this[obj.name].object);
      //     this[obj.name] = new obj.class();
      //     this.sceneObjects.add(this[obj.name].object);
      //   });
      // }

      module.hot.accept('./modules/lights', () => {
        while (this.lights.object.children.length) this.lights.object.remove(this.lights.object.children[0]);
        this.sceneObjects.remove(this.lights.object);
        this.lights = new Lights();
        this.sceneObjects.add(this.lights.object);
      });

      this.hands.setScene(this.sceneObjects);

      module.hot.accept('./modules/vr-console', () => {
        while (this.vrconsole.object.children.length) this.vrconsole.object.remove(this.vrconsole.object.children[0]);
        this.sceneObjects.remove(this.vrconsole.object);
        this.vrconsole = new VRConsole();
        this.sceneObjects.add(this.vrconsole.object);
      });

      module.hot.accept('./modules/toy', () => {
        while (this.toy.object.children.length) this.toy.object.remove(this.toy.object.children[0]);
        this.sceneObjects.remove(this.toy.object);
        this.toy = new Toy();
        this.sceneObjects.add(this.toy.object);
      });

      module.hot.accept('./modules/desk', () => {
        while (this.desk.object.children.length) this.desk.object.remove(this.desk.object.children[0]);
        this.sceneObjects.remove(this.desk.object);
        this.desk = new Desk();
        this.sceneObjects.add(this.desk.object);
      });

      module.hot.accept('./modules/keyboard', () => {
        while (this.keyboard.object.children.length) this.keyboard.object.remove(this.keyboard.object.children[0]);
        this.sceneObjects.remove(this.keyboard.object);
        this.keyboard = new Keyboard();
        this.sceneObjects.add(this.keyboard.object);
      });

      module.hot.accept('./modules/monitor', () => {
        while (this.monitor.object.children.length) this.monitor.object.remove(this.monitor.object.children[0]);
        this.sceneObjects.remove(this.monitor.object);
        this.monitor = new Monitor();
        this.sceneObjects.add(this.monitor.object);
      });

      module.hot.accept('./modules/hands', () => {
        this.hands = new Hands();
        this.hands.setHands(this.handsArray);
        this.hands.setScene(this.sceneObjects);
      });
    }

    window.addEventListener('resize', this.resize, false);
    // window.addEventListener('keydown', this.keydown);
    // window.addEventListener('keyup', this.keyup);

    this.animate();
  }

  keydown({ key }) {
    if (window.desktop.connection.socket) window.desktop.connection.socket.emit('keydown', { data: key });
  }

  keyup({ key }) {
    if (window.desktop.connection.socket) window.desktop.connection.socket.emit('keyup', { data: key });
  }

  resize() {
    this.camera.aspect = window.innerWidth / window.innerHeight;
    this.camera.updateProjectionMatrix();

    this.renderer.setSize(window.innerWidth, window.innerHeight);
  }

  render() {
    this.headset.position.copy(this.camera.position);
    this.headset.quaternion.copy(this.camera.quaternion);

    this.renderer.render(this.scene, this.camera);
    this.sceneModules.forEach((sceneObject) => {
      if (this[sceneObject.name].update) this[sceneObject.name].update();
    });
  }

  animate() {
    this.renderer.setAnimationLoop(this.render);
  }

  // poll while waiting for hand models to load:
  getHandMeshes() {
    return new Promise((resolve) => {
      const promises = [this.getLeftHandMesh(), this.getRightHandMesh(0)];
      Promise.all(promises).then((res) => {
        resolve(res);
      });
    });
  }

  getLeftHandMesh() {
    return new Promise((resolve) => {
      this.waitForHand1 = setInterval(() => {
        const mesh = this.hand1.getObjectByProperty('type', 'SkinnedMesh');
        if (mesh !== undefined) {
          this.hand1.mesh = mesh;
          clearInterval(this.waitForHand1);
          resolve();
        }
      }, 100);
    });
  }

  getRightHandMesh() {
    return new Promise((resolve) => {
      this.waitForHand2 = setInterval(() => {
        const mesh = this.hand2.getObjectByProperty('type', 'SkinnedMesh');
        if (mesh !== undefined) {
          this.hand2.mesh = mesh;
          clearInterval(this.waitForHand2);
          resolve();
        }
      }, 100);
    });
  }
}

const app = new App();
if (window.location.origin.indexOf('localhost') >= 0) window.app = app;
document.body.appendChild(app.el);
